({
    printMe: function (component, event, helper) {
        window.print();
    },
    init: function (component, event, helper) {
        component.set('v.entryId', component.get('v.pageReference').state.c__entryId);
        component.set('v.wrapperName', component.get('v.pageReference').state.c__wrapperName);
        var compoName = component.get('v.wrapperName');
        $A.createComponent('c:' + compoName, {
            'entryId': component.get('v.entryId')
        }, function (newButton, status, errorMessage) {
            //Add the new button to the body array
            if (status === 'SUCCESS') {
                var firstPanel = component.get('v.componenttoshow');
                firstPanel.push(newButton);
                component.set('v.componenttoshow', firstPanel);
            }
        });
    }
});
