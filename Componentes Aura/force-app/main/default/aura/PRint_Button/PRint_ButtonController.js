({
    init: function (component, event, helper) {},
    handleClick: function (component, event, helper) {
        var hostname = window.location.hostname;
        var arr = hostname.split('.');
        var instance = arr[0];
        window.open('https://' + hostname + '/lightning/cmp/c__PRint_Wrapper' + '?c__entryId=' + component.get('v.recordId') + '&c__wrapperName=' + component.get('v.wrapperName'));
    }
});